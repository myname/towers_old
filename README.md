# Заметки

[Играть онлайн](http://147.45.135.117:3001/)

* Одна карта
* 5 видов монстров: - обычный, медленный (много ХП), быстрый (мало ХП), матерый (быстрый и много ХП, но), босс
    * обычный - все по стандарту
    * медленный - выходят толпой, их больше и у них больше ХП
    * быстрый - мало ХП, быстрые и их меньше
    * матерый - средняя скорость, их мало и у них много ХП
    * БОСС - их всего два и очень много ХП
* Бессконечные волны монстров (кружков)
* С каждой волной монстры становятся сильнее в процентном соотношении (если правильно помню)
* 4 вида башен
* 6 уровней развиия башен
* В зависимости от типа башни растет их урон, радиус, скорость и особые возможности
* Для поулчения подсказки кликайте на вопросик возле башни (во время строительства)
* Если пропустите 10 кружков, то игра закончится
* Нажми на монстра что бы увидеть информацию о нем
* Каждые 10 волн идет два босса
* До какого уровня дошли?

Не залипайте на долго!