'use strict';
console.log(`\n\n\nSTART SERVER SITE!\n\n`) // ВСЕ ГЛОБАЛЬЫНЕ ПЕРЕМЕННЫЕ СЛЕДОВАЛО БЫ СДЕЛАТЬ КАПСОМ

const path = require('path')
const fs = require('fs')
const mainPath = require.main.path
const express = require('express')
const server = express()



server.use(async (request, response, next) => {
    try {
        response.removeHeader('X-Powered-By') // удаляем запись express js
        response.setHeader('X-Frame-Options', 'DENY') // защита от iframe
        response.setHeader('X-Content-Type-Options', 'nosniff') // MMI type - защита крч

        return next()
    } catch(error) {
        console.log(`\n\n\tОшибка server.use:`, error, '\n')
    }
})




server.use('/js', express.static(path.resolve(mainPath, './js/')))
server.use('/style', express.static(path.resolve(mainPath, './style/')))
server.use('/img', express.static(path.resolve(mainPath, './img/')))
server.use('/sound', express.static(path.resolve(mainPath, './sound/')))
// server.use('/favicon.ico', express.static(path.resolve(mainPath, './dist/favicon.ico')))
// server.use('/robots.txt', express.static(path.resolve(mainPath, './dist/robots.txt')))
// server.use(express.json()) // хрень что бы post запросы ловили данные

server.disable('cross-origin-embedder-policy') // разрешаем загрузку изображений с других доменов



const template = fs.readFileSync(path.join(mainPath, 'index.html'), 'utf-8') // по идее тут бы дождаться окончания загрузки, но не обязательно
server.get('*', (request, response) => {
    response.set({
        'Content-Type': 'text/html' // text/plain
    })

    return response.send(template)
})



server.listen(3001, () => {
    console.log(`Server running`)
})

