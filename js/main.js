;'use script';

sound_1.addEventListener('input', (e) => {
	// console.log(e.target.value)
	g.soundVolume = e.target.value / 100
})

sound_2.addEventListener('input', (e) => {
	// console.log(e.target.value)
	g.soundVolumeMusic = e.target.value / 100
})


;(function() {
	var lastTime = 0;
	var vendors = ['ms', 'moz', 'webkit', 'o'];
	for(var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
		window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
		window.cancelAnimationFrame = window[vendors[x]+'CancelAnimationFrame'] 
		|| window[vendors[x]+'CancelRequestAnimationFrame'];
	}
	if (!window.requestAnimationFrame)
	window.requestAnimationFrame = function(callback, element) {
		var currTime = new Date().getTime();
		var timeToCall = Math.max(0, 16 - (currTime - lastTime));
		var id = window.setTimeout(function() { callback(currTime + timeToCall); }, 
		timeToCall);
		lastTime = currTime + timeToCall;
		return id;
	};
	if (!window.cancelAnimationFrame)
	window.cancelAnimationFrame = function(id) {
		clearTimeout(id);
	};
}());


var imgTest = new Image();
imgTest.src = 'img/test.png';

var towersImg = new Image();
towersImg.src='img/towers.png';
var ctx = cn.getContext('2d'),
	g = {
	sound: function(sound){ // всегда создает новый запрос, сделать без запросов, но с созданием нового проигрывания
		var audio = new Audio();
		audio.src = 'sound/'+sound+'.mp3';
		audio.volume=g.soundVolume;
		audio.play();
	},
	soundVolume: 1,
	soundVolumeMusic: 1,
	mob: [
		{ // обычный
			speed: 1,
			color: 'Silver',
			radius: 10,
			heal: 100,
			gold: 5
		},
		{ // медленный
			speed: 0.7,
			color: 'Green',
			radius: 10,
			heal: 160,
			gold: 7
		},
		{ // быстрый
			speed: 1.7,
			color: 'Gold',
			radius: 10,
			heal: 60,
			gold: 9
		},
		{ // матерый
			speed: 1.3,
			color: 'OrangeRed',
			radius: 10,
			heal: 130,
			gold: 14
		},
		{ // босс
			speed: 0.7,
			color: 'DarkRed',
			radius: 10,
			heal: 1500,
			gold: 250
		}
	],
	towers: [
		{ // стрелковая
			name: 0,
			speed: 750, // скорость стрельбы ms
			sd: 7, // скорость полета снаряда
			color: 'DarkKhaki',
			img: {x:0,y:0},
			radius: 170,
			dmg: 130,
			mass: false,
			gold: 100,
			sound: 'tower',
			upgrade: [{img:{x:0,y:0},gold:100},
				{dmg:270,radius:180,speed:700,gold:240,img:{y:0,x:60}},
				{dmg:680,radius:200,speed:650,gold:490,img:{y:0,x:120}},
				{dmg:1600,radius:220,speed:600,gold:800,img:{y:0,x:180}},
				{dmg:4200,radius:240,speed:550,gold:1900,img:{y:0,x:240}},
				{dmg:10000,radius:270,speed:500,gold:6000,img:{y:0,x:300}},
				{dmg:0,radius:0,gold:0,img:{y:0,x:360}}]
		},
		{ // пушка
			name: 1,
			speed: 1500,
			sd: 100,
			color: 'PaleGreen',
			img: {y:60,x:0},
			imgEffect: {x1:150,y1:270,x2:240,y2:270}, // эффекты
			radius: 90,
			dmg: 80,
			mass: true,
			dec: 0.9, // замедление
			decTime: 600, // время замедления
			gold: 120,
			sound: 'lightning',
			upgrade: [{img:{y:60,x:0},gold:120},
				{dmg:220,radius:95,gold:290,decTime:700,img: {y:60,x:60}},
				{dmg:550,radius:100,gold:620,dec:0.8,decTime:850,img:{y:60,x:120}},
				{dmg:1300,radius:105,gold:1500,dec:0.7,decTime:1000,img:{y:60,x:180}},
				{dmg:3100,radius:110,gold:3100,dec:0.6,decTime:1200,img:{y:60,x:240}},
				{dmg:7400,radius:120,gold:5800,dec:0.4,decTime:1500,img:{y:60,x:300}},
				{dmg:0,radius:0,gold:0,img: {y:60,x:360}}]
		},
		{ // лазерная
			name: 2,
			speed: 300,
			sd: 20,
			color: 'DeepSkyBlue',
			img: {y:120,x:0},
			imgEffect: {x1:180,y1:240,x2:270,y2:240},
			radius: 120,
			dmg: 70,
			critical: 1.5, // множитель крита
			chanse: 5, // шанс крита
			mass: false,
			gold: 140,
			sound: 'laser',
			upgrade: [{img:{y:120,x:0},gold:140},
				{dmg:130,speed: 270,radius:125,gold:290,chanse:9,img:{y:120,x:60}},
				{dmg:380,speed:250,radius:130,gold:500,chanse:13,critical:2,img:{y:120,x:120}},
				{dmg:900,speed:200,radius:135,gold:1800,chanse:17,critical:2.5,img:{y:120,x:180}},
				{dmg:2500,speed:180,radius:140,gold:3200,chanse:21,critical:3,img:{y:120,x:240}},
				{dmg:6200,speed:145,radius:160,gold:7000,chanse:33,critical:4,img:{y:120,x:300}},
				{dmg:0,speed:180,radius:0,gold:0,img: {y:120,x:360}}]
		},
		{ // снайперская
			name: 3,
			speed: 5000,
			sd: 15,
			color: 'Indigo',
			img: {y:180,x:0},
			imgEffect: {x1:180,y1:270}, // эффекты
			radius: 250,
			dmg: 1500,
			dmgP: 0.02, // урон в %
			mass: false,
			gold: 250,
			sound: 'fire',
			upgrade: [{img:{y:180,x:0},gold:250},
				{dmg:4000,radius:260,gold:450,dmgP:0.03,img:{y:180,x:60}},
				{dmg:9000,radius:270,gold:1100,dmgP:0.04,img:{y:180,x:120}},
				{dmg:25000,radius:290,gold:2500,dmgP:0.05,img:{y:180,x:180}},
				{dmg:50000,radius:300,gold:6700,dmgP:0.07,img:{y:180,x:240}},
				{dmg:150000,radius:305,gold:12000,dmgP:0.1,img:{y:180,x:300}},
				{dmg:0,radius:0,gold:0,img:{y:180,x:360}}]
		}
	],
	sell: function(t){var g=0;for(var i=0;i<t.lvl;i++){g+=t.upgrade[i].gold;}return g*0.8;}, // продажа
	gold: 200,
	dead: 0, // дошедшие
	kill: 0, // убитые
	dell: 0, // очищенные
	lvl: 0, // волна мобов
	start: true, // активация кнопки старт
	runMob: [[0,29,500],[1,24,500],[2,19,250],[3,14,500],[0,29,500],[3,19,500],[1,29,200],[2,19,250],[0,29,300],[4,1,1000]], // инфа о мобах на каждой волне - тип,число,интервал
	positionMob: [], // массив мобов
	mobPowHP: function(hp){ // hp - стандартное хп
		if(g.lvl%10==9){for(var i=0;i<g.lvl/5;i++){hp*=1.52;}return Math.ceil(hp*g.lvl);} // если босс то увеличим его хп ==9 так как лвл еще не успел ++
		for(var i=0;i<g.lvl;i++){hp+=5;hp*=1.085;}return Math.ceil(hp*g.lvl)||hp; // хп для мобов
	},
	mobRoad1: [ // чекпоинты пути мобов
		{x:50,y:10,r:10},
		{x:50,y:100,r:10},
		{x:200,y:275,r:10},
		{x:400,y:275,r:10},
		{x:600,y:50,r:10},
		{x:820,y:300,r:10},
		{x:660,y:500,r:10},
		{x:480,y:370,r:10},
		{x:100,y:370,r:10},
		{x:300,y:550,r:10},
		{x:920,y:550,r:10},
		{x:920,y:10,r:10,end:true},
		{x:920,y:10,r:10,end:true}
	],
	drawBuyMap1: [ // места башен для карты 1
		{x:600,y:200,img:{x:0,y:240}},
		{x:600,y:250,img:{x:0,y:240}},
		{x:800,y:200,img:{x:0,y:240}},
		{x:800,y:350,img:{x:0,y:240}},
		{x:650,y:300,img:{x:0,y:240}},
		{x:700,y:300,img:{x:0,y:240}},
		{x:700,y:250,img:{x:0,y:240}},
		{x:800,y:400,img:{x:0,y:240}},
		{x:100,y:250,img:{x:0,y:240}},
		{x:100,y:300,img:{x:0,y:240}},
		{x:150,y:300,img:{x:0,y:240}},
		{x:200,y:300,img:{x:0,y:240}},
		{x:250,y:300,img:{x:0,y:240}},
		{x:300,y:300,img:{x:0,y:240}},
		{x:350,y:300,img:{x:0,y:240}},
		{x:400,y:300,img:{x:0,y:240}},
		{x:450,y:300,img:{x:0,y:240}},
		{x:450,y:250,img:{x:0,y:240}},
		{x:500,y:250,img:{x:0,y:240}},
		{x:500,y:200,img:{x:0,y:240}},
		{x:550,y:250,img:{x:0,y:240}},
		{x:550,y:200,img:{x:0,y:240}},
		{x:300,y:200,img:{x:0,y:240}},
		{x:600,y:300,img:{x:0,y:240}},
		{x:620,y:400,img:{x:0,y:240}},
		{x:450,y:400,img:{x:0,y:240}}
	],
	startMob: function(mob,mRoad){ // запуск моба по дороге
		if(!mob&&!mRoad)return; // если не передан mob и mRoad то выход
		var mobRoad = mRoad,m = (function(){return { // перезаписываем в локальную, добавляя параметры
			speed: mob.speed,
			color: mob.color,
			radius: mob.radius,
			heal: g.mobPowHP(mob.heal),
			healDmg: g.mobPowHP(mob.heal),
			gold: mob.gold+g.lvl*2,
			menu: false,
			deceleration: 1, // замедление
			dec: false, // эффект замедления
		}})(),
			x=0,y=10, // координаты начала
			to=g.goTo(0,10,50,10,m.speed), // направление
			n=m.radius*2, // отображение хп
			heal=m.heal;  // отображение хп

		if(g.lvl%10==0){m.gold*=g.lvl/5;} // увеличиваем цену за боссов

		return (function animation(){
			if (g.startAudio) g.startAudio.volume = g.soundVolumeMusic;

			if (g.end) return;
			if(m.heal<=0){return}; // при смерти выходим

			n=(20*m.heal)/heal; // отображение хп

			ctx.beginPath(); // рисовка новых элементов
			ctx.fillStyle = m.color;
			ctx.arc(x,y,m.radius,0,2*Math.PI);
			ctx.fill(); // заливка

			ctx.beginPath(); // отображение хп
			ctx.strokeStyle = 'green';
			if(m.dec){ctx.strokeStyle = 'orange';} // эффект замедления
			ctx.lineWidth = 2;
			ctx.moveTo(x-m.radius, y-15);
			ctx.lineTo(x-m.radius+n, y-15);
			ctx.stroke();

			if(m.menu){ // радиус
				ctx.beginPath();
				ctx.strokeStyle='red';
				if(m.dec){ctx.strokeStyle = 'orange';} // эффект замедления
				ctx.arc(x,y,11,0,2*Math.PI);
				ctx.stroke();
			}

			for(var i=0;i<mobRoad.length;i++){
				if(g.checkInt(x,y,mobRoad[i].x,mobRoad[i].y,mobRoad[i].r)){ // если дошел до чекпоинта
					to=g.goTo(x,y,mobRoad[i+1].x,mobRoad[i+1].y,m.speed);
					if(mobRoad[i].end){ // если дошел моб
						var audio = new Audio();
						audio.src = 'sound/kick.mp3';
						audio.volume=g.soundVolume;
						audio.play();

						g.dead++; // увеличиваем список дошедших
						m.menu=false; // закрываем меню
						m.heal=m.healDmg=m.radius=m.color=m.speed=x=y=undefined; // удаляем данные

						if (g.dead >= 10) {
							// если больше 10 дошло
							g.startAudio.loop = false;
							g.startAudio.pause();

							var audio = new Audio();
							audio.src = 'sound/end.mp3';
							audio.volume=g.soundVolume;
							audio.play();

							g.end = true;

							g.start=true;
							g.positionMob=[];
							g.drawStart(); // рисуем кнопку старта

							setTimeout(() => {
								alert('Вы проиграли, ваш уровень: ' + g.lvl);
								window.location = window.location;
							}, 100);
							return;
						}

						if(!g.checkMob()){g.start=true;g.positionMob=[];} // если мобы мертвы то включаем кнопку и обнуляем массив
						g.drawStart(); // рисуем кнопку старта
						return;
					}
					break; // что бы не проверяло все, когда уже нашло
				}
			}

			x-=to.x*m.deceleration;
			y-=to.y*m.deceleration;

			window.requestAnimationFrame(animation);
			return {
				deceleration: function(s,t){
					if(!s||!t)return m.deceleration; // если нет параметров
					m.dec=true; // включаем эффект замедления
					var sDec=s; // запоминаем
					m.deceleration*=s; // замедляем
					setTimeout(function(){ // возвращаем врежнюю скорость через t
						m.deceleration/=sDec;
						if(m.deceleration>=0.9){m.dec=false;} // убираем эффект по окончанию действия замедления (баги с deceleration ибо 0.1+0.2!=0.3)
					},t);
				},
				gold: function(){return m.gold},
				color: m.color,
				menu: function(par){
					if(typeof(par)==="boolean")m.menu=par;
					return m.menu;
				},
				x: function(){return x;},
				y: function(){return y;},
				speed: function(){return m.speed*m.deceleration;}, // учитывая замедление
				healDmg: function(dmg){
					dmg=dmg||0;
					m.healDmg-=dmg=Math.abs(dmg);
					if(m.healDmg<=0){ // если смерть
						this.healDmg=function(){return 0;};
						return 0;
					}
					return m.healDmg;
				},
				heal: function(dmg){
					dmg=dmg||0;
					m.heal-=dmg=Math.abs(dmg);
					if(m.heal<=0){ // если смерть то убираем данные в массиве
						g.kill++; // увеличиваем счетчик убитых
						g.gold+=m.gold; // зачисляем деньги
						this.menu(false); // закрываем меню
						this.x=this.y=this.speed=this.heal=this.healDmg=function(){return undefined;}; // удаляем данные
						if(!g.checkMob()){g.start=true;g.positionMob=[];} // если мобы мертвы то включаем кнопку и обнуляем массив
						g.drawStart(); // рисуем кнопку старта
						return 0;
					}
					return m.heal;
				}
			}
		})();
	},
	startRound: function(){ // запуск раунда (тип,число,интервал,путь)
		g.end = false;
		var audio = new Audio();
		audio.src = 'sound/start.mp3';
		audio.volume=g.soundVolumeMusic;
		audio.loop = true;
		audio.play();
		g.startAudio = audio;

		g.start=false; // убираем кнопку
		var i=0,j=0,par=g.runMob[g.lvl%10]; // 10 видов повторяются
		//var mob=g.mob[par[0]];
		//mob.heal=g.mobPowHP(mob.heal); // заменяем хп за ранее, оптимизация
		g.lvl++; // увеличиваем лвл волны мобов
		var int = setInterval(function(){
			g.positionMob[g.positionMob.length] = g.startMob(g.mob[par[0]],g.mobRoad1);if(i==par[1]){clearInterval(int);}++i;
		},par[2]);
	},
	addOpt: function(to,opt){for(key in opt){to[key]=opt[key];}return g.addOpt;},
	goTo: function(x1,y1,x2,y2,speed){
		var c = Math.sqrt(Math.pow((x1-x2),2)+Math.pow((y1-y2),2)); // гипотенуза или расстояние до цели
		var n = c/speed; // кол-во шагов
		return {x: (x1-x2)/n,y: (y1-y2)/n}
	},
	checkInt: function(x1,y1,x2,y2,r){ // чек пересечения кругов
		if(!x1||!x2||!y1||!y2||!r)return false;
		if(Math.sqrt(Math.pow((x1-x2),2)+Math.pow((y1-y2),2))>=r)return false;return true; // true-пересекаются
	},
	checkMob: function(){ // проверяет остались ли мобы
		for(var i=0;i<g.positionMob.length;i++){ // не запускается если массив пустой
			if(!!g.positionMob[i].x()&&!!g.positionMob[i].heal()){return true;}
		}
		return false;
	},
	fillTower: function(tower){ // создание башни
		var checkAttack=false,x=tower.x,y=tower.y;
		(function animation(){
			if(!tower.upg)return; // фикс бага продажи и остатка там башни, (множественный урон)
			ctx.beginPath(); // отрисовка башни и радиуса
			ctx.drawImage(towersImg,tower.img.x,tower.img.y,60,60,x,y,45,45); // где 30 это высота и ширина башни
			if(tower.menu){
				ctx.lineWidth = 1; // толщина радиуса
				ctx.strokeStyle = tower.color;
				ctx.arc(x+22.5,y+22.5,tower.radius,0,2*Math.PI); // где 22.5 это половина ширины или высоты башни
				ctx.stroke();
			}
			if(!checkAttack){ // если атаки нет (доп проверка, что бы цикл зря не гонял)
				for(var i=0;i<g.positionMob.length;i++){ // проверка на вхождение в радиус атаки
					if(g.checkInt(x+22.5,y+22.5,g.positionMob[i].x(),g.positionMob[i].y(),tower.radius)&&g.positionMob[i].healDmg()>0){ // если радиус и есть жизни
						if(!checkAttack||tower.mass){ // если атаки нет - активируем (для избежания массовых атак)
							if(tower.chanse>Math.ceil(Math.random()*100)){var allDmg=tower.critical*tower.dmg;} // крит урон
							if(tower.dec){g.positionMob[i].deceleration(tower.dec,tower.decTime);} // замедление
							if(tower.dmgP){var allDmg=tower.dmg+g.positionMob[i].heal()*tower.dmgP;} // соединяем урон и урон в % если он есть
							g.positionMob[i].healDmg(allDmg||tower.dmg); // наносим урон по мобу за ранее, что бы избежать лишних атак других башен
							g.goAttack(x+22.5,y+22.5,g.positionMob[i],tower.sd,allDmg||tower.dmg); // x и у - координаты вылета снаряда
							
							if(!checkAttack){ // фикс бага множества атак по 1 и тому же мобу у массовой башни
								g.sound(tower.sound); // проигрываем звук выстрела, 1 раз
								setTimeout(function(){checkAttack=false;},tower.speed); // скорость стрельбы
								checkAttack=true;
							}
						}
					}
				}
			}
			window.requestAnimationFrame(animation);
		})();
	},
	goAttack: function(x1,y1,mob,speed,dmg){ // выпуск снаряда
		if(speed>=50){mob.heal(dmg);return;} // мгновенный урон при большой скорости
		(function animation(){
			var x2=mob.x(),y2=mob.y();
			var go = g.goTo(x1,y1,x2,y2,speed);

			ctx.beginPath();

			ctx.drawImage(imgTest,0,0,20,20,x1,y1,15,15);

			//ctx.arc(x1,y1,3,0,2*Math.PI);
			//ctx.fillStyle = 'blue';
			//ctx.fill();

			if(g.checkInt(x1,y1,x2,y2,12)){ // столкновение
				mob.heal(dmg);return; // урон по хилу фейковому, что бы моб исчез
			}
			x1-=go.x;
			y1-=go.y;
			window.requestAnimationFrame(animation);
		})();
	},
	drawBuy: function(mass){ // создание полей для покупки башен
		function fill(t){
			var x=t.x,y=t.y;
			(function animation(){
				ctx.beginPath();
				ctx.lineWidth = 2;
				//ctx.fillStyle = 'black';
				//ctx.fillRect(x,y,45,45); // где 30 это высота и ширина
				ctx.drawImage(towersImg,t.img.x,t.img.y,60,60,x,y,45,45);
				if(t.menu){ctx.strokeStyle = 'red';ctx.strokeRect(x-1,y-1,47,47);} // подсветка башни
				window.requestAnimationFrame(animation);
				//func()()()...
			})();
		}
		for(var i=0;i<mass.length;i++){fill(mass[i]);}
	},
	drawMenu: function(tower){
		g.drawMenu.active=tower; // записываем выделенную башню (ссылка на обьект)
		if(tower.upg){ // если улучшение
			var p=165; // отступ
			(function animation(){
				if(!tower.menu)return; // закрываем
				ctx.beginPath();
				ctx.font = 'bold 15px arial';
				ctx.lineWidth = 2;

				//добавить в отрисовку карты
				ctx.fillStyle = '#eee';
				ctx.fillRect(120,560,670,70);
				ctx.fillStyle = 'black';
				ctx.fillRect(120,560,670,2);
				ctx.fillRect(120,560,2,70);
				ctx.fillRect(790,560,2,70);

				ctx.fillStyle = tower.color;
				var img = tower.upgrade[tower.lvl-1].img;
				var img2 = tower.upgrade[tower.lvl].img;
				ctx.drawImage(towersImg,img.x,img.y,60,60,10+p,565,60,60);
				ctx.drawImage(towersImg,img2.x,img2.y,60,60,55+p*3,565,60,60);

				ctx.fillStyle = 'black';
				ctx.fillText(tower.dmg, 190+p,580); // x+30
				ctx.drawImage(towersImg,120,240,30,30,160+p,565,20,20); // y-15
				ctx.fillText(tower.radius,190+p,600);
				ctx.drawImage(towersImg,150,240,30,30,160+p,585,20,20);
				ctx.fillText((1000/tower.speed).toFixed(3).slice(0,-1), 190+p,620);
				ctx.drawImage(towersImg,210,240,30,30,160+p,605,20,20);

				if(tower.imgEffect){ // доп эффекты башни
					ctx.fillText(tower.critical||tower.dec||tower.dmgP,270+p,580);
					ctx.fillText(tower.upgrade[tower.lvl].critical||tower.upgrade[tower.lvl].dec||tower.upgrade[tower.lvl].dmgP||tower.critical||tower.dec||tower.dmgP,225+p*3,580);

					if(Object.keys(tower.imgEffect).length>=4){ // если параметров несколько
						ctx.fillText(tower.chanse||tower.decTime,270+p,600);
						ctx.fillText(tower.upgrade[tower.lvl].chanse||tower.upgrade[tower.lvl].decTime||tower.chanse||tower.decTime,225+p*3,600);
						ctx.drawImage(towersImg,tower.imgEffect.x2,tower.imgEffect.y2,30,30,240+p,585,20,20);
						ctx.drawImage(towersImg,tower.imgEffect.x2,tower.imgEffect.y2,30,30,195+p*3,585,20,20);
					}
					ctx.drawImage(towersImg,tower.imgEffect.x1,tower.imgEffect.y1,30,30,240+p,565,20,20);
					ctx.drawImage(towersImg,tower.imgEffect.x1,tower.imgEffect.y1,30,30,195+p*3,565,20,20);
				}
				ctx.fillText((tower.upgrade[tower.lvl].dmg||tower.dmg),145+p*3,580);
				ctx.drawImage(towersImg,120,240,30,30,115+p*3,565,20,20);
				ctx.fillText((tower.upgrade[tower.lvl].radius||tower.radius),145+p*3,600);
				ctx.drawImage(towersImg,150,240,30,30,115+p*3,585,20,20);
				ctx.fillText((1000/(tower.upgrade[tower.lvl].speed||tower.speed)).toFixed(3).slice(0,-1),145+p*3,620);
				ctx.drawImage(towersImg,210,240,30,30,115+p*3,605,20,20);

				ctx.fillStyle=ctx.strokeStyle = 'red';
				ctx.fillText(tower.lvl,1+p,621);
				ctx.fillText(tower.lvl+1,46+p*3,621);
				ctx.font = 'bold 22px arial';
				ctx.strokeRect(72+p,597,27,27);
				ctx.fillText("$",80+p,618);
				ctx.fillText(g.sell(tower),105+p,618);
				ctx.fillStyle=ctx.strokeStyle = 'green';
				ctx.strokeRect(72+p,566,27,27);
				ctx.fillText("\u2713",78+p,587);
				ctx.fillText((tower.upgrade[tower.lvl].gold||tower.gold),105+p,587);
				ctx.font = 'bold 35px arial';
				ctx.fillText("\u21E8",155+p*2,605); // стрелочка

				window.requestAnimationFrame(animation);
			})();
		}else if(tower.menu){ // если покупка
			var p=220; // отступ
			(function animation(){
				if(!tower.menu)return; // закрываем
				ctx.beginPath();

				//добавить в отрисовку карты
				ctx.fillStyle = '#eee';
				ctx.fillRect(0,560,p*4,70);

				for(var i=0;i<g.towers.length;i++){
					//добавить в отрисовку карты
					ctx.fillStyle = 'black';
					ctx.fillRect(880,560,2,70);
					ctx.fillRect(4+p*i,560,2,70);
					ctx.fillRect(p*i,560,220,2);

					ctx.lineWidth = 2;
					ctx.strokeStyle = 'green';
					ctx.font = 'bold 15px arial';
					ctx.fillStyle = g.towers[i].color;

					//ctx.fillRect(10+p*i,570,30,30);
					ctx.drawImage(towersImg,g.towers[i].img.x,g.towers[i].img.y,60,60,10+p*i,565,60,60);

					ctx.strokeRect(75+p*i,601,20,20);
					ctx.fillStyle = 'red';
					ctx.fillText("?",80+p*i,617);
					ctx.fillStyle = 'black';
					ctx.fillText(g.towers[i].dmg,175+p*i,580);
					ctx.drawImage(towersImg,120,240,30,30,145+p*i,565,20,20);
					ctx.fillText(g.towers[i].radius,175+p*i,600);
					ctx.drawImage(towersImg,150,240,30,30,145+p*i,585,20,20);
					ctx.fillText((1000/g.towers[i].speed).toFixed(3).slice(0,-1),175+p*i,620);
					ctx.drawImage(towersImg,210,240,30,30,145+p*i,605,20,20);

					ctx.fillStyle=ctx.strokeStyle = 'green';
					ctx.font = 'bold 18px arial';
					ctx.fillText(g.towers[i].gold,103+p*i,585.6);
					ctx.font = 'bold 22px arial';
					ctx.strokeRect(73+p*i,568,24,24);
					ctx.fillText("\u2713",76+p*i,588);
					
					if(g.towers[i].imgEffect){ctx.drawImage(towersImg,g.towers[i].imgEffect.x1,g.towers[i].imgEffect.y1,30,30,105+p*i,601,20,20);} // доп эффект
				}
				window.requestAnimationFrame(animation);
			})();
		}
	},
	drawStart: function(){ // рисовка кнопки старт
		(function animation(){
			if(!g.start)return;
			if (g.startAudio) { // конец проигрывания аудио в конце
				g.startAudio.loop = false;
				g.startAudio.pause();
			}

			ctx.beginPath();
			ctx.fillStyle='#eee';
			ctx.fillRect(890,570,100,50);
			ctx.fillStyle='red';
			ctx.font = 'bold 25px arial';
			ctx.fillText("Start",910,604)

			window.requestAnimationFrame(animation);
		})();
	},
	drawMenuMob: function(mob){ // рисуем меню инфы о мобе
		(function animation(){
			if(!mob.menu())return; // закрываем меню
			ctx.beginPath();
			ctx.fillStyle=mob.color;
			ctx.arc(200,600,20,0,2*Math.PI);
			ctx.fill(); // заливка

			ctx.font = 'bold 22px arial';
			ctx.fillStyle='red';
			ctx.fillText((mob.speed().toFixed(2)||0),290,608);
			ctx.drawImage(towersImg,120,270,30,30,250,586,30,30);
			ctx.fillText((mob.gold()||0),410,608);
			ctx.drawImage(towersImg,210,270,30,30,370,586,30,30);
			ctx.fillText(Math.ceil(mob.heal()||0),530,608);
			ctx.drawImage(towersImg,240,240,30,30,490,586,30,30);
			window.requestAnimationFrame(animation);
		})();
	},
	drawMap: function(){
		(function animation(){
			//if()return; // если смена карты
			ctx.beginPath();
			ctx.fillStyle = 'black';
			ctx.font = 'bold 22px arial';
			ctx.fillText(g.kill,340,30);
			ctx.drawImage(towersImg,300,270,30,30,300,8,30,30);
			ctx.fillText(g.dead,440,30);
			ctx.drawImage(towersImg,300,240,30,30,400,8,30,30);
			ctx.fillText(g.lvl,540,30);
			ctx.drawImage(towersImg,330,240,30,30,500,8,30,30);
			ctx.fillText(g.gold,640,30);
			ctx.drawImage(towersImg,210,270,30,30,600,8,30,30);
			window.requestAnimationFrame(animation);
		})();
	}
};


g.drawBuy(g.drawBuyMap1); // рисуем места покупки
g.drawStart(); // рисуем кнопку старта
g.drawMap(); // рисуем карту
setInterval(function(){ctx.clearRect(0,0,cn.scrollWidth,cn.scrollHeight);},100/9); // 90 fps, чистим

cn.onclick = function(e){
	var x2=e.offsetX,y2=e.offsetY,t=g.drawMenu.active||{}; // координаты клика
	if(g.start){ // первым делом проверяем была ли нажата кнопка старт
		if(890<=x2&&570<=y2&&990>=x2&&620>=y2){
			//console.log('start');
			g.startRound(); // запуск мобов
		}
	}

	if(t.menu){ // если открыто меню
		var p=165;
		if(t.upg){ // если улучшение сначала обрабатываем его
			if(72+p<=x2&&597<=y2&&99+p>=x2&&624>=y2){ // продажа
				g.gold+=g.sell(t); // зачисление денег
				g.sound('sell');
				t.img={x:0,y:240}; // ставим картинку без башни
				for(var key in t){
					if(key=='x'||key=='y'||key=='menu'||key=='img')continue;
					delete t[key]; // удаляем лишнее
				}
			}
			if(72+p<=x2&&566<=y2&&99+p>=x2&&593>=y2){ // улучшаем
				var gold=g.towers[t.name].upgrade[t.lvl].gold;
				if(g.gold<gold||t.lvl>=6)return; // если 6 лвл то не улучшаем дальше(переделать) || нет денег
				g.gold-=gold; // снимаем деньги
				g.sound('buy');
				g.addOpt(t,g.towers[t.name].upgrade[t.lvl]); // обновляем параметры башни
				++t.lvl; // увеличиваем лвл
				return; // для не скидывания выделенной башни после улучшения
			}
		}else{ // клики по меню(не уулчшение)
			var p=220;
			for(var i=0;i<g.towers.length;i++){
				if(73+p*i<=x2&&568<=y2&&97+p*i>=x2&&592>=y2){ // если клик по покупке
					var gold=g.towers[i].gold;
					if(g.gold<gold)return; // если не хватает денег то выход
					g.gold-=gold; // снимаем деньги
					g.sound('buy');
					g.addOpt(t,g.towers[i])(t,{lvl: 1,upg: true}); // добавляем опции
					g.fillTower(t); // ставим башню
				}
				if(75+p*i<=x2&&601<=y2&&95+p*i>=x2&&621>=y2){ // если клик по вопросику
					switch(i){
						case 0: alert('Имеет большой радиус, средний урон, среднюю скорость атаки.\nХороша для охвата больших территорий.');break;
						case 1: alert('Имея маленький радиус поражает все цели находящиеся в нем.\nИмеет большой урон и маленькую скорость атаки.\nХороша против медленных групп.\nДоп эффект - замедление.');break;
						case 2: alert('Имеет не большой радиус, большую скорострельность и средний урон.\nХороша для быстрого нанесения урона.\nДоп эффект - крит.');break;
						case 3: alert('Имеет большой радиус и огромный урон, однако очень маленькую скорость стрельбы.\nХороша для нанесения мгновенного урона.\nДоп эффект - наносит доп урон от % жизней.');break;
					}
					return; // для не скидывания выделенной башни после "?"
				}
			}
		}
	}
	for(var i=0;i<g.drawBuyMap1.length;i++){ // проверка на клик по башне или покупающему месту
		var x=g.drawBuyMap1[i].x;y=g.drawBuyMap1[i].y;
		if(x<=x2&&x+45>=x2&&y<=y2&&y+45>=y2){ // если нашли совпадения
			if(g.drawBuyMap1[i].menu)return; // если уже активно то выход
			g.drawBuyMap1[i].menu=true;
			g.drawMenu(g.drawBuyMap1[i]); // показываем меню покупки
			continue; // пропускаем что бы не выключить окно
		}
		g.drawBuyMap1[i].menu=false; // выключаем все окна
	}
	var oneMob=true; // для выделения только одного моба
	for(var i=0;i<g.positionMob.length;i++){ // проверка на клик по мобу
		var x=g.positionMob[i].x();y=g.positionMob[i].y();
		if(g.checkInt(x,y,x2,y2,10)&&oneMob){ // если попали по мобу
			oneMob=false;
			g.positionMob[i].menu(true);
			g.drawMenuMob(g.positionMob[i]); // рисуем меню
			continue; // пропускаем что бы не выключить окно
		}
		g.positionMob[i].menu(false);
	}
}

window.onkeydown=function(e){
	var t=g.drawMenu.active||{};
	if(t.menu&&t.upg){ // если меню активно и это улучшение
		if(e.keyCode==68){ // d
			var gold=g.towers[t.name].upgrade[t.lvl].gold;
			if(g.gold<gold||t.lvl>=6)return; // если 6 лвл то не улучшаем дальше(переделать) || нет денег
			g.gold-=gold; // снимаем деньги
			g.sound('buy');
			g.addOpt(t,g.towers[t.name].upgrade[t.lvl]); // обновляем параметры башни
			++t.lvl; // увеличиваем лвл
		}
		if(e.keyCode==83){ // s
			g.gold+=g.sell(t); // зачисление денег
			g.sound('sell');
			t.img={x:0,y:240}; // ставим картинку без башни
			t.menu=false; // закрываем меню, избежание ошибок
			for(var key in t){
				if(key=='x'||key=='y'||key=='menu'||key=='img')continue;
				delete t[key]; // удаляем лишнее
			}
		}
	}else if(t.menu){ // если покупка
		switch(e.keyCode){
			case 49: // 1
				var gold=g.towers[0].gold;
				if(g.gold<gold)break; // если не хватает денег то выход
				t.menu=false; // закрываем меню
				g.gold-=gold; // снимаем деньги
				g.sound('buy');
				g.addOpt(t,g.towers[0])(t,{lvl: 1,upg: true}); // добавляем опции
				g.fillTower(t); // ставим башню
				break;
			case 50: // 2
				var gold=g.towers[1].gold;
				if(g.gold<gold)break;
				t.menu=false;
				g.gold-=gold;
				g.sound('buy');
				g.addOpt(t,g.towers[1])(t,{lvl: 1,upg: true});
				g.fillTower(t);
				break;
			case 51: // 3
				var gold=g.towers[2].gold;
				if(g.gold<gold)break;
				t.menu=false;
				g.gold-=gold;
				g.sound('buy');
				g.addOpt(t,g.towers[2])(t,{lvl: 1,upg: true});
				g.fillTower(t);
				break;
			case 52: // 4
				var gold=g.towers[3].gold;
				if(g.gold<gold)break;
				t.menu=false;
				g.gold-=gold;
				g.sound('buy');
				g.addOpt(t,g.towers[3])(t,{lvl: 1,upg: true});
				g.fillTower(t);
				break;
		}
	}
}

window.onload=function(){
	cn.style.marginLeft=(document.body.clientWidth-cn.clientWidth)/2-1; // 1-border
}
window.onresize=function(){
	cn.style.marginLeft=(document.body.clientWidth-cn.clientWidth)/2-1; // 1-border
}

/*
цикл проверки можно обьеденить между всеми башнями, проверяя каждого моба сразу у всех башен и отсылая атаку преедавая башню j
атаку по выбранной цели
сделать сколько живых

сортировать массив с мобами для определения кто первый и удаления мертвых

показывать перезарядку башни

что если клики делать не по канвасу а по диву сверху, а он будет ловить клики и передавать в канвас, так легче определять положение мыши и статических обьектов

урон от недостающего хп
стаканье урона, каждый следующий наносит больше

вместо меча рисовать снаряд этой башни

баг хп у мобов на 6 волне, 4к хп.. если запускать волны постепенно
*/
